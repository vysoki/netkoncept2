## Getting started

## Testowanko 123

### Installation:

Install dependencies using `yarn` package manager

### `yarn`

### Environment variables
```sh
REACT_APP_PORTAL_DEV_API_URL=https://api.example.com/
REACT_APP_SPA_URL=https://spa.example.com/
```

Create .env.development.local and .env.production.local files, one for development and second for production build.
# Netkoncept Szkolenie GIT

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn prettier`
Launches prettier to fix code formatting.

### `yarn lint-check`
Launches linter to fix code style.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles Application in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.